'use strict';

const enabled = !['false', '0'].includes(process.env.CACHE_ENABLED);

const types = {
    milliseconds: 'MILLISECOND',
    seconds: 'SECOND',
    minutes: 'MINUTE',
    hours: 'HOUR',
    days: 'DAY',
    months: 'MONTH',
    years: 'YEAR'
};

class AbstractCache {
    constructor () {
        this.isEnabled = enabled;
    }

    static get types () {
        return types;
    }

    prepareKey (key) {
        return key.toLowerCase();
    }
}

module.exports = AbstractCache;
