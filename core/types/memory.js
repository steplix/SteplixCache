'use strict';

const _ = require('lodash');
const P = require('bluebird');
const minimatch = require('minimatch');
const memoryCache = require('memory-cache');
const Base = require('./abstract');

class Cache extends Base {
    constructor () {
        super();

        if (this.isEnabled) {
            this.client = new memoryCache.Cache();
        }
    }

    get (key, defaultValue) {
        if (!this.isEnabled) {
            return P.resolve(defaultValue);
        }

        return P.bind(this)
            .then(() => this.client.get(key))
            .then(value => {
                if (!_.isNil(value)) {
                    return JSON.parse(value);
                }
                return defaultValue;
            });
    }

    put (key, value, time) {
        if (!this.isEnabled) {
            return P.resolve(value);
        }

        if (_.isObject(key)) {
            time = value;
        }

        if (!_.isObject(key)) {
            if (_.isNil(value)) {
                return this.remove(key);
            }
            else {
                key = {
                    [key]: value
                };
            }
        }

        let p = P.bind(this);

        _.each(key, (v, k) => {
            p = p.then(() => {
                return this.client.put(k, JSON.stringify(v), time);
            });
        });
        return p.then(() => {
            return value;
        });
    }

    remove (key) {
        if (!this.isEnabled) {
            return P.resolve();
        }

        if (!_.isArray(key)) {
            key = [key];
        }

        let p = P.bind(this);

        _.each(key, (v, k) => {
            p = p.then(() => {
                return this.client.del(key);
            });
        });
        return p;
    }

    removeMatch (pattern) {
        if (!this.isEnabled) {
            return P.resolve();
        }

        const keys = this.client.keys();
        let p = P.bind(this);

        _.each(keys, key => {
            if (minimatch(key, pattern)) {
                p = p.then(() => {
                    return this.client.del(key);
                });
            }
        });
        return p;
    }

    clear () {
        if (!this.isEnabled) {
            return P.resolve();
        }

        return P.bind(this)
            .then(() => this.client.clear());
    }
}

module.exports = Cache;
