# Cache

steplix-cache is a Node.js cache helper.


## Index

* [Download & Install][install].
* [How is it used?][how_is_it_used].
  + [Middleware][how_is_it_used_middleware].
* [Tests][tests].


## Download & Install

### NPM
For use with **redis**
```bash
    npm install steplix-cache redis
```

For use with **memory cache**
```bash
    npm install steplix-cache memory-cache
```

### Source code
```bash
$ git clone https://gitlab.com/comodinx/cache.git
$ cd cache
$ npm install
```


## How is it used?

### Configure

| Environment variable      | Value         | Default value |
|:--------------------------|:--------------|:--------------|
| CACHE_ENABLED             | true/false    | `true`        |
| CACHE_TYPE                | memory/redis  | `'memory'`    |


#### Only for Redis

| Environment variable      | Default value | Value         |
|:--------------------------|:--------------|:--------------|
| CACHE_HOST                | redis host    | localhost     |
| CACHE_PORT                | redis port    | 6379          |


#### Only for API cache middleware

| Environment variable      | Value                     | Default value     |
|:--------------------------|:--------------------------|:------------------|
| CACHE_MIDDLEWARE_DURATION | number unit               | `'1 hour'`        |
| CACHE_HEADER_CONTROL      | cache control header name | `'cache-control'` |


```js
const cache = require('steplix-cache');

// Simple usage
await cache.put('foo', 'bar');
const foo = await cache.get('foo');
console.log(foo);

// Expiration time usage
await cache.put('cash', 'yes', 100);
const cash = await cache.get('cash');
console.log('I have cash? ' + cash);

setTimeout(() => {
  const cash = await cache.get('cash');
  console.log('I have cash? ' + cash);
}, 200);

// Print on console:
//
// bar
// I have cash? yes
// I have cash? undefined
```


## Middleware

**Inspirated on [apicache](https://www.npmjs.com/package/apicache)**

API response caching middleware for Express/Node.

### Simple usage

```js
const { apicache } = require('steplix-cache');
const express = require('express');
const app = express();
const port = 3000;

app.use(apicache('1 hour'));

app.get('/', (req, res) => {
    res.send({ foo: 'bar' });
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
```

Or at a specific endpoint.

```js
const { apicache } = require('steplix-cache');
const express = require('express');
const app = express();
const port = 3000;

app.get('/', apicache('1 hour'), (req, res) => {
    res.send({ foo: 'bar' });
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
```

### Time format

`{number} {unit time}`

| Available unit time |
|:--------------------|
| `m      / ms`       |
| `minute / minutes`  |
| `hour   / hours`    |
| `day    / days`     |
| `week   / weeks`    |
| `month  / months`   |

### Available options

```js
{
  enabled:             process.env.CACHE_ENABLED,                           // if false or undefined, turns off caching globally (useful on dev)
  defaultDuration:     process.env.CACHE_MIDDLEWARE_DURATION || '1 hour',   // should be either a number (in ms) or a string, defaults to '1 hour'
  headerCacheControl:  process.env.CACHE_HEADER_CONTROL || 'cache-control', // should be either a string, defaults to 'cache-control'
  respectCacheControl: process.env.CACHE_RESPECT_HEADER_CONTROL,            // should be either a boolean (true | 1), defaults to false
  cacheKey:            req.originalUrl || req.url,                          // should be either a strong or a function (in function case, return a string custom key), defaults to req.originalUrl || req.url
  statusCode:          {                                                    // list of status codes that should never be cached
    include: [],
    exclude: []
  },
  headerBlacklist:     []                                                   // list of headers that should never be cached
}
```

### Other ways of use

Use `apicache` with custom options
```js
app.get('/', apicache(), (req, res) => /* ... */);

// OR

app.get('/', apicache('2 hours', 'my-custom-cache-key'), (req, res) => /* ... */);

// OR

app.get(
    '/',
    apicache({
        duration: '2 hours'
        cacheKey: 'my-custom-cache-key'
    }),
    (req, res) => /* ... */
);
```


For only cache success responses, use `apicache.ok` (status code between 200 - 299)
```js
app.get('/', apicache.ok(), (req, res) => /* ... */);

// OR

app.get('/', apicache.ok('2 hours', 'my-custom-cache-key'), (req, res) => /* ... */);

// OR

app.get(
    '/',
    apicache.ok({
        duration: '2 hours'
        cacheKey: 'my-custom-cache-key'
    }),
    (req, res) => /* ... */
);
```


For only cache success responses, use `apicache.ok` (status code between 200 - 299)
```js
app.get('/', apicache.ok(), (req, res) => /* ... */);

// OR

app.get('/', apicache.ok('2 hours', 'my-custom-cache-key'), (req, res) => /* ... */);

// OR

app.get(
    '/',
    apicache.ok({
        duration: '2 hours'
        cacheKey: 'my-custom-cache-key'
    }),
    (req, res) => /* ... */
);
```


## Tests

In order to see more concrete examples, **I INVITE YOU TO LOOK AT THE TESTS :)**

### Run the unit tests
```sh
npm test
```

<!-- deep links -->
[install]: #download--install
[how_is_it_used]: #how-is-it-used
[how_is_it_used_middleware]: #middleware
[tests]: #tests
