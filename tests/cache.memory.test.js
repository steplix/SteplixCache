'use strict';

/* eslint-disable no-unused-expressions */

const _ = require('lodash');
const cache = require('../core/steplix');
const Cache = cache.memory;

const CACHE_KEY_FOR_TEST = 'test.cache.check';
const CACHE_VALUE_FOR_TEST = {
    value: 123
};

describe('@comodinx/cache', () => {
    describe('memory', () => {
        let cache;

        beforeEach(() => {
            cache = new Cache();
        });

        afterEach(() => {
            cache = null;
        });

        it('Verifies isEnabled', () => {
            expect(cache.isEnabled).to.be.true;
        });

        describe('#get', () => {
            it('Should get an undefined value', async () => {
                try {
                    const cached = await cache.get(CACHE_KEY_FOR_TEST);

                    expect(cached).to.be.undefined;
                }
                catch (e) {
                    // Ignore
                }
            });

            it('Should get an undefined value, but instead will get a default value', async () => {
                try {
                    const cached = await cache.get(CACHE_KEY_FOR_TEST, CACHE_VALUE_FOR_TEST.value);

                    expect(cached).to.equal(CACHE_VALUE_FOR_TEST.value);
                }
                catch (e) {
                    // Ignore
                }
            });
        });

        describe('#put', () => {
            it('Should put an example value', async () => {
                try {
                    let cached = await cache.put(CACHE_KEY_FOR_TEST, CACHE_VALUE_FOR_TEST);

                    expect(_.isEqual(cached, CACHE_VALUE_FOR_TEST)).to.be.true;
                    cached = await cache.get(CACHE_KEY_FOR_TEST);
                    expect(_.isEqual(cached, CACHE_VALUE_FOR_TEST)).to.be.true;
                }
                catch (e) {
                    // Ignore
                }
            });

            it('Should remove an key when value is undefined ', async () => {
                try {
                    let cached = await cache.put(CACHE_KEY_FOR_TEST, CACHE_VALUE_FOR_TEST);

                    expect(_.isEqual(cached, CACHE_VALUE_FOR_TEST)).to.be.true;
                    cached = await cache.put(CACHE_KEY_FOR_TEST);
                    expect(_.isEqual(cached, CACHE_VALUE_FOR_TEST)).to.be.false;
                    cached = await cache.get(CACHE_KEY_FOR_TEST);
                    expect(cached).to.be.undefined;
                }
                catch (e) {
                    // Ignore
                }
            });

            it('Should get an expired value, but instead will get a undefined value', done => {
                (async () => {
                    let cached = await cache.put(CACHE_KEY_FOR_TEST, CACHE_VALUE_FOR_TEST, 150);

                    expect(_.isEqual(cached, CACHE_VALUE_FOR_TEST)).to.be.true;
                    cached = await cache.get(CACHE_KEY_FOR_TEST);
                    expect(_.isEqual(cached, CACHE_VALUE_FOR_TEST)).to.be.true;

                    (async () => setTimeout(async () => {
                        try {
                            cached = await cache.get(CACHE_KEY_FOR_TEST);
                            expect(_.isEqual(cached, CACHE_VALUE_FOR_TEST)).to.be.false;
                        }
                        catch (e) {
                            // Ignore
                        }

                        done();
                    }, 151))();
                })();
            });
        });

        describe('#remove', () => {
            it('Should remove an key', async () => {
                try {
                    let cached = await cache.put(CACHE_KEY_FOR_TEST, CACHE_VALUE_FOR_TEST);

                    expect(_.isEqual(cached, CACHE_VALUE_FOR_TEST)).to.be.true;
                    await cache.remove(CACHE_KEY_FOR_TEST);
                    cached = await cache.get(CACHE_KEY_FOR_TEST);
                    expect(cached).to.be.undefined;
                }
                catch (e) {
                    // Ignore
                }
            });

            it('Should remove all match keys', async () => {
                try {
                    await cache.put({
                        [`${CACHE_KEY_FOR_TEST}1`]: 1,
                        [`${CACHE_KEY_FOR_TEST}2`]: 2,
                        [`${CACHE_KEY_FOR_TEST}3`]: 3,
                        [`${CACHE_KEY_FOR_TEST}4`]: 4,
                        [`${CACHE_KEY_FOR_TEST}5`]: 5,
                        other_key: 6
                    });

                    let cached = await cache.get(`${CACHE_KEY_FOR_TEST}1`);

                    expect(cached).to.equal(1);
                    await cache.removeMatch(`${CACHE_KEY_FOR_TEST}*`);
                    cached = await cache.get(`${CACHE_KEY_FOR_TEST}1`);
                    expect(cached).to.be.undefined;
                    cached = await cache.get('other_key');
                    expect(cached).to.equal(6);
                }
                catch (e) {
                    // Ignore
                }
            });

            it('Should clear cache', async () => {
                try {
                    await cache.put({
                        [`${CACHE_KEY_FOR_TEST}1`]: 1,
                        [`${CACHE_KEY_FOR_TEST}2`]: 2,
                        [`${CACHE_KEY_FOR_TEST}3`]: 3,
                        [`${CACHE_KEY_FOR_TEST}4`]: 4,
                        [`${CACHE_KEY_FOR_TEST}5`]: 5
                    });

                    let cached = await cache.get(`${CACHE_KEY_FOR_TEST}1`);

                    expect(cached).to.equal(1);
                    await cache.clear();
                    cached = await cache.get(`${CACHE_KEY_FOR_TEST}1`);
                    expect(cached).to.be.undefined;
                }
                catch (e) {
                    // Ignore
                }
            });
        });
    });
});

/* eslint-enable no-unused-expressions */